#!/usr/bin/make -f

all: formatlist.rst

formatlist.rst: scripts/makesummary arkivformater/*.yaml
	scripts/makesummary arkivformater/*.yaml > $@

clean:
	rm *~ */*~
