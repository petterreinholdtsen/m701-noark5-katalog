M701-verdikatalog for Noark 5 Tjenestegrensesnitt
=================================================

Maskinlesbar liste med arkivformatkoder og tilhørende navn og
MIME-type.  Dette er felt M701 i Noark 5-standarden.  Listen brukes
for å sikre samvirke mellom arkivløsninger, slik at arkivdata kan
flyttes mellom arkivløsninger uten å måtte kode om dokumentmetadata.

Alle formater oppført i "`Forskrift om utfyllende
tekniske og arkivfaglige bestemmelser om behandling av offentlige
arkiver (riksarkivarens forskrift)
<https://lovdata.no/dokument/SF/forskrift/2017-12-19-2286>`_" § 5-17
gis prefikset 'RA-'.

MIME-type er i henhold til `IANAs autorative lister over MIME-typer
<https://www.iana.org/assignments/media-types/media-types.xhtml>`_,
der dette eksisterer.

I metakatalogen for Noark 5 versjon 4.0 publisert 2017-10-10 står det
at faste verdier for M701 format bestemmes senere (side 60).  Det
samme står i versjon 3.1 publisert 2013-03-15.  I versjon 2.0 utgitt
2009-04-03 er M701 definert uten å nevne noe om når verdier skal
fastsettes.  Det er på tide å få fastsatt felles sett med verdier for
dette feltet.

Under utarbeidelse av Noark 5 Tjenestegrensesnitt er det blitt klart
at Arkivverket har bestemt at `PRONOM-identifikatorer skal brukes
<https://www.nationalarchives.gov.uk/PRONOM/Default.aspx>`_ (se
mangelmeldingene
`#4 <https://github.com/arkivverket/noark5-tjenestegrensesnitt-standard/issues/4>`_,
`#11 <https://github.com/arkivverket/noark5-tjenestegrensesnitt-standard/issues/11>`_
og
`#25 <https://github.com/arkivverket/noark5-tjenestegrensesnitt-standard/issues/25>`_).

Hvis noe mangler i denne katalogen eller bør endres, ta kontakt og
registrer en endringsforespørsel på
https://codeberg.org/noark/m701-noark5-katalog .

Katalogoppføringer legges inn på `YAML-formatet <http://yaml.org/>`_,
og kan for eksempel se slik ut::

  Kode:        RA-JPEG
  PRONOM:      fmt/41 fmt/42 fmt/43 fmt/44
  Navn:        JPEG
  MIME-type:   image/jpeg
  Opprettet:   2018-11-08
  Endret:      2018-11-10
  Kilde:       NOARK 5 tjenestegrensesnitt versjon 1.0 beta
  Merknad: >
   JPEG (ISO 10918-1:1994).

Koden er verdien som brukes i felt M701/format.  Navnet er en tekstlig
beskrivelse av format.  MIME-type er mediatypen fra `IANAs register
over mediatyper
<https://www.iana.org/assignments/media-types/media-types.xhtml>`_ som
brukes for formatet.  Opprettet er datoen når formatkoden ble lagt inn
i denne katalogen.  Endret er dato for når denne oppføringen ble sist
endret.  Kilde er en referanse til hvor formatkoden ble påtruffet
første gang, eller hvem som foreslo/sendte inn katalogoppføringen.
Merknad er en tekstlig beskrivelse av formatet, henvisninger til
filformatbeskrivelser, standarder etc.  Merk at '>' er valgritt, men
påkrevd for a jobbe rundt uheldig oppførsel fra YAML når
merkadsteksten inneholder ': '.
